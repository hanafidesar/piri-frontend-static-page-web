import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// component
import { HelpComponent } from './main/help/help.component';
import { AkunDanPengaturanComponent } from './main/help/detail/akundanpengaturan-help/akundanpengaturan-help.component';
import { TopupComponent } from './main/topup/topup.component';
import { AboutComponent } from './main/about/about.component';

import { TidakBisaMasukAplikasiComponent } from './main/help/detail/akundanpengaturan-help/detail/tidak-bisa-masuk-aplikasi/tidak-bisa-masuk-aplikasi.component';
import { LupaPasswordComponent } from './main/help/detail/akundanpengaturan-help/detail/lupa-password/lupa-password.component';
import { UpdateInformasiAkunComponent } from './main/help/detail/akundanpengaturan-help/detail/update-informasi-akun/update-informasi-akun.component';

import { PemesananComponent } from './main/help/detail/pemesanan/pemesanan.component';
import { TidakBisaLihatOutletComponent } from './main/help/detail/pemesanan/detail/tidak-bisa-lihat-outlet/tidak-bisa-lihat-outlet.component';
import { TidakBisaPesanComponent } from './main/help/detail/pemesanan/detail/tidak-bisa-pesan/tidak-bisa-pesan.component';

import { VoucherPembayaranComponent } from './main/help/detail/voucher-pembayaran/voucher-pembayaran.component';
import { ApaPiripayComponent } from './main/help/detail/voucher-pembayaran/detail/apa-piripay/apa-piripay.component';
import { CaraIsiSaldoComponent } from './main/help/detail/voucher-pembayaran/detail/cara-isi-saldo/cara-isi-saldo.component';
import { CaraMenggunakanPiripayComponent } from './main/help/detail/voucher-pembayaran/detail/cara-menggunakan-piripay/cara-menggunakan-piripay.component';
import { KeuntunganPiripayComponent } from './main/help/detail/voucher-pembayaran/detail/keuntungan-piripay/keuntungan-piripay.component';
import { PiripayBisaDigabungComponent } from './main/help/detail/voucher-pembayaran/detail/piripay-bisa-digabung/piripay-bisa-digabung.component';


import { TermOfServiceComponent } from './main/about/detail/term-of-service/term-of-service.component';
import { PrivacyPolicyComponent } from './main/about/detail/privacy-policy/privacy-policy.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'about' },

  { path: 'help', component: HelpComponent },
  
  { path: 'akun-pengaturan-help', component: AkunDanPengaturanComponent },
  { path: 'tidak-masuk-aplikasi', component: TidakBisaMasukAplikasiComponent },
  { path: 'lupa-password', component: LupaPasswordComponent },
  { path: 'update-informasi-akun', component: UpdateInformasiAkunComponent },

  { path: 'pemesanan', component: PemesananComponent },
  { path: 'tidak-bisa-lihat-outlet', component: TidakBisaLihatOutletComponent },
  { path: 'tidak-bisa-pesan', component: TidakBisaPesanComponent },

  { path: 'voucher-pembayaran', component: VoucherPembayaranComponent },
  { path: 'apa-itu-piripay', component: ApaPiripayComponent },
  { path: 'menggunakan-piripay', component:  CaraMenggunakanPiripayComponent},
  { path: 'mengisi-piripay', component:  CaraIsiSaldoComponent},
  { path: 'keuntungan-piripay', component: KeuntunganPiripayComponent },
  { path: 'digabung-piripay', component: PiripayBisaDigabungComponent },

  { path: 'term-of-service', component: TermOfServiceComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },

  { path: 'topup', component: TopupComponent },
  { path: 'about', component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [HelpComponent, AboutComponent]
