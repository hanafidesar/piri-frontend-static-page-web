import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-topup',
  templateUrl: './topup.component.html',
  styleUrls: ['./topup.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TopupComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
