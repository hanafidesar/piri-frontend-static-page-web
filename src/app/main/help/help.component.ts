import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  akundanpengaturan() {
    this.router.navigate(['akun-pengaturan-help']);
  }

  pemesanan() {
    this.router.navigate(['pemesanan']);
  }

  voucherPembayaran() {
    this.router.navigate(['voucher-pembayaran']);
  }
} 
