import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-voucher-pembayaran',
  templateUrl: './voucher-pembayaran.component.html',
  styleUrls: ['./voucher-pembayaran.component.css']
})
export class VoucherPembayaranComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  apaPiriPay() {
    this.router.navigate(['apa-itu-piripay']);
  }

  menggunakanPiripay() {
    this.router.navigate(['menggunakan-piripay']);
  }

  mengisiPiripay() {
    this.router.navigate(['mengisi-piripay']);
  }

  keuntunganPiripay() {
    this.router.navigate(['keuntungan-piripay']);
  }

  digabungPiripay() {
    this.router.navigate(['digabung-piripay']);
  }
}
