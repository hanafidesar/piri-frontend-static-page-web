import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApaPiripayComponent } from './apa-piripay.component';

describe('ApaPiripayComponent', () => {
  let component: ApaPiripayComponent;
  let fixture: ComponentFixture<ApaPiripayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApaPiripayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApaPiripayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
