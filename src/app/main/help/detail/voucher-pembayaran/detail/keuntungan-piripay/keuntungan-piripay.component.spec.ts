import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeuntunganPiripayComponent } from './keuntungan-piripay.component';

describe('KeuntunganPiripayComponent', () => {
  let component: KeuntunganPiripayComponent;
  let fixture: ComponentFixture<KeuntunganPiripayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeuntunganPiripayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeuntunganPiripayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
