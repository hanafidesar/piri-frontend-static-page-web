import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaraIsiSaldoComponent } from './cara-isi-saldo.component';

describe('CaraIsiSaldoComponent', () => {
  let component: CaraIsiSaldoComponent;
  let fixture: ComponentFixture<CaraIsiSaldoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaraIsiSaldoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaraIsiSaldoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
