import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaraMenggunakanPiripayComponent } from './cara-menggunakan-piripay.component';

describe('CaraMenggunakanPiripayComponent', () => {
  let component: CaraMenggunakanPiripayComponent;
  let fixture: ComponentFixture<CaraMenggunakanPiripayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaraMenggunakanPiripayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaraMenggunakanPiripayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
