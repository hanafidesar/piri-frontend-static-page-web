import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiripayBisaDigabungComponent } from './piripay-bisa-digabung.component';

describe('PiripayBisaDigabungComponent', () => {
  let component: PiripayBisaDigabungComponent;
  let fixture: ComponentFixture<PiripayBisaDigabungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiripayBisaDigabungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiripayBisaDigabungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
