import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherPembayaranComponent } from './voucher-pembayaran.component';

describe('VoucherPembayaranComponent', () => {
  let component: VoucherPembayaranComponent;
  let fixture: ComponentFixture<VoucherPembayaranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherPembayaranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherPembayaranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
