import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TidakBisaPesanComponent } from './tidak-bisa-pesan.component';

describe('TidakBisaPesanComponent', () => {
  let component: TidakBisaPesanComponent;
  let fixture: ComponentFixture<TidakBisaPesanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TidakBisaPesanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TidakBisaPesanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
