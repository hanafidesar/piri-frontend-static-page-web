import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TidakBisaLihatOutletComponent } from './tidak-bisa-lihat-outlet.component';

describe('TidakBisaLihatOutletComponent', () => {
  let component: TidakBisaLihatOutletComponent;
  let fixture: ComponentFixture<TidakBisaLihatOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TidakBisaLihatOutletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TidakBisaLihatOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
