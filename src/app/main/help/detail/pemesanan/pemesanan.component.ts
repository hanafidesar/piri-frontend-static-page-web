import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-pemesanan',
  templateUrl: './pemesanan.component.html',
  styleUrls: ['./pemesanan.component.css']
})
export class PemesananComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  tidakMelihatDaftarOutlet() {
    this.router.navigate(['tidak-bisa-lihat-outlet']);
  }

  tidakBisaPesan() {
    this.router.navigate(['tidak-bisa-pesan']);
  }

}
