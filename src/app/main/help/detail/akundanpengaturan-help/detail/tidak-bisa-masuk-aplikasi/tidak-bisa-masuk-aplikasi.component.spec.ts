import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TidakBisaMasukAplikasiComponent } from './tidak-bisa-masuk-aplikasi.component';

describe('TidakBisaMasukAplikasiComponent', () => {
  let component: TidakBisaMasukAplikasiComponent;
  let fixture: ComponentFixture<TidakBisaMasukAplikasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TidakBisaMasukAplikasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TidakBisaMasukAplikasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
