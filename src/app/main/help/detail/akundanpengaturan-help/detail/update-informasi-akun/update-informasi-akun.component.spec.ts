import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateInformasiAkunComponent } from './update-informasi-akun.component';

describe('UpdateInformasiAkunComponent', () => {
  let component: UpdateInformasiAkunComponent;
  let fixture: ComponentFixture<UpdateInformasiAkunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateInformasiAkunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateInformasiAkunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
