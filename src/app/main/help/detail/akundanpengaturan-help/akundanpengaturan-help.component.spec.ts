import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AkunDanPengaturanComponent } from './akundanpengaturan-help.component';

describe('FirstHelpComponent', () => {
  let component: AkunDanPengaturanComponent;
  let fixture: ComponentFixture<AkunDanPengaturanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AkunDanPengaturanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AkunDanPengaturanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
