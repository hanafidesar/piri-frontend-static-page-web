import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-akundanpengaturan-help',
  templateUrl: './akundanpengaturan-help.component.html',
  styleUrls: ['./akundanpengaturan-help.component.css']
})
export class AkunDanPengaturanComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  tidakMasukAplikasi(){
    this.router.navigate(['tidak-masuk-aplikasi']);
  }

  lupaPassword(){
    this.router.navigate(['lupa-password']);
  }

  updateInformasiAkun(){
    this.router.navigate(['update-informasi-akun']);
  }
}
