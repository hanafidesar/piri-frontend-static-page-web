import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  showSpinner: boolean;
  constructor(public router: Router) { }

  ngOnInit() {
  }

  openFacebook() {
    window.location.href = 'https://www.facebook.com/piri.io';
  }

  openTweeter() {
    window.location.href = 'https://twitter.com/piri_io';
  }

  openInstagram() {
    window.location.href = 'https://www.instagram.com/piri.io/?hl=id';
  }
  termOfService() {
    this.router.navigate(['term-of-service']);
  }

  privacyPolicy() {
    this.router.navigate(['privacy-policy']);
  }
}
