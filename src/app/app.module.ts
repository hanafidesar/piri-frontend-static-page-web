// import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { FormsModule } from '@angular/forms';
// import { HttpModule } from '@angular/http';
import { SharedModule } from '../../node_modules/primeng/components/common/shared.js';

import { AccordionModule } from 'primeng/components/accordion/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { routingComponent, AppRoutingModule } from './app-routing.module';
import { HelpComponent } from './main/help/help.component';
import { TopupComponent } from './main/topup/topup.component';

import { AboutComponent } from './main/about/about.component';

import { AkunDanPengaturanComponent } from './main/help/detail/akundanpengaturan-help/akundanpengaturan-help.component';
import { TidakBisaMasukAplikasiComponent } from './main/help/detail/akundanpengaturan-help/detail/tidak-bisa-masuk-aplikasi/tidak-bisa-masuk-aplikasi.component';
import { LupaPasswordComponent } from './main/help/detail/akundanpengaturan-help/detail/lupa-password/lupa-password.component';
import { UpdateInformasiAkunComponent } from './main/help/detail/akundanpengaturan-help/detail/update-informasi-akun/update-informasi-akun.component';

import { PemesananComponent } from './main/help/detail/pemesanan/pemesanan.component';
import { TidakBisaLihatOutletComponent } from './main/help/detail/pemesanan/detail/tidak-bisa-lihat-outlet/tidak-bisa-lihat-outlet.component';
import { TidakBisaPesanComponent } from './main/help/detail/pemesanan/detail/tidak-bisa-pesan/tidak-bisa-pesan.component';

import { VoucherPembayaranComponent } from './main/help/detail/voucher-pembayaran/voucher-pembayaran.component';
import { ApaPiripayComponent } from './main/help/detail/voucher-pembayaran/detail/apa-piripay/apa-piripay.component';
import { CaraIsiSaldoComponent } from './main/help/detail/voucher-pembayaran/detail/cara-isi-saldo/cara-isi-saldo.component';
import { CaraMenggunakanPiripayComponent } from './main/help/detail/voucher-pembayaran/detail/cara-menggunakan-piripay/cara-menggunakan-piripay.component';
import { KeuntunganPiripayComponent } from './main/help/detail/voucher-pembayaran/detail/keuntungan-piripay/keuntungan-piripay.component';
import { PiripayBisaDigabungComponent } from './main/help/detail/voucher-pembayaran/detail/piripay-bisa-digabung/piripay-bisa-digabung.component';

import { TermOfServiceComponent } from './main/about/detail/term-of-service/term-of-service.component';
import { PrivacyPolicyComponent } from './main/about/detail/privacy-policy/privacy-policy.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponent,
    HelpComponent,
    TopupComponent,
    AboutComponent,
    AkunDanPengaturanComponent,
    TidakBisaMasukAplikasiComponent,
    LupaPasswordComponent,
    UpdateInformasiAkunComponent,
    PemesananComponent,
    TidakBisaLihatOutletComponent,
    TidakBisaPesanComponent,
    TermOfServiceComponent,
    PrivacyPolicyComponent,
    VoucherPembayaranComponent,
    ApaPiripayComponent,
    CaraIsiSaldoComponent,
    CaraMenggunakanPiripayComponent,
    KeuntunganPiripayComponent,
    PiripayBisaDigabungComponent
  ],
  imports: [
    SharedModule,
    BrowserAnimationsModule,
    AccordionModule,
    // BrowserModule,
    // FormsModule,
    // HttpModule,
    AppRoutingModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
