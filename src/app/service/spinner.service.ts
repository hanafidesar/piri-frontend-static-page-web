import { Injectable } from '@angular/core';

@Injectable()
export class SpinnerService {
    selector: any;
    spinnerElement = `
        <div class="spinner" style="text-align:center;">
            <div class="spinner-center" style="margin-top:300px;">
            <i class="fa fa-circle-o-notch fa-spin" style="font-size:64px"></i>
            </div>
        </div>
    `;

    show(selector: string) {
        $(selector).css({
            //   this.selector.css({
            position: 'relative',
            display: 'block',
            'min-height': '100vh'
        }).append(this.spinnerElement);

        return this.hide.bind(this, selector);
    }

    hide(selector: string) {
        $(selector).find('.spinner').remove();
        //this.selector.find('.spinner').remove();
    }
}