import { StaticPagePage } from './app.po';

describe('static-page App', () => {
  let page: StaticPagePage;

  beforeEach(() => {
    page = new StaticPagePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
